using System;
using System.Collections.Generic;
using Fijo.Code.SimpleOOPLang.Dto;
using Fijo.Code.SimpleOOPLang.Services;

namespace Fijo.Code.SimpleOOPLangTest {
	public class NativeMock : Native {
		private readonly Action<bool> _print;

		public NativeMock(Action<bool> print) {
			_print = print;
		}

		public override Expression Execute(string function, IList<Expression> arguments) {
			if(function == "print") {
				_print(InstanceToBool((Instance) arguments[0]));
				return null;
			}
			return base.Execute(function, arguments);
		}
	}
}