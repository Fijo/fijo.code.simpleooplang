﻿using System.Collections.Generic;
using Fijo.Code.SimpleOOPLang.Dto;
using Fijo.Code.SimpleOOPLang.Services;
using NUnit.Framework;

namespace Fijo.Code.SimpleOOPLangTest {
	[TestFixture]
	public class SimpleTest {
		[Test]
		public void Test() {
			var printed = new List<bool>();
			var @do = new Do(new NativeMock(printed.Add));
			var assembly = GetAssembly();
			@do.ExecuteAssembly(assembly);
			var wanted = new List<bool> {false};
			CollectionAssert.AreEqual(wanted, printed);
		}

		private Assembly GetAssembly() {
			var trueClass = new Class();
			var falseClass = new Class();
			trueClass.Content = GetBoolClassContent("__native_bool_true", trueClass, falseClass);
			falseClass.Content = GetBoolClassContent("__native_bool_false", falseClass, trueClass);
			var assembly = new Assembly(new Call(new NativeFunc("print"), new Call(trueClass.Content["__equals"], falseClass)));
			return assembly;
		}

		private IDictionary<string, Expression> GetBoolClassContent(string nativeBoolFieldName, Expression trueEqualExpression, Expression falseEqualExpression) {
			return new Dictionary<string, Expression>
			{
				{nativeBoolFieldName, GetVoidExpression()},
				{"__equals", GetEqualsFunction(trueEqualExpression, falseEqualExpression)}
			};
		}

		private NativeFunc GetVoidExpression() {
			return new NativeFunc("void");
		}

		private Expression GetEqualsFunction(Expression trueEqualExpression, Expression falseEqualExpression) {
			return new Function(new List<string> {"obj"},
			                    new Call(new NativeFunc("set"),
			                             new Variable("__return"),
			                             new Call(new NativeFunc("if"), new Variable("obj"),
			                                      trueEqualExpression,
			                                      falseEqualExpression))
				);
		}
	}
}
