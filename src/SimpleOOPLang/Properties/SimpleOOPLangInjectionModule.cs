﻿using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Code.SimpleOOPLang.Properties {
	public class SimpleOOPLangInjectionModule : ExtendedNinjectModule {
		public override void OnLoad(IKernel kernel) {
		}
	}
}