using System.Collections.Generic;
using Fijo.Code.SimpleOOPLang.Dto;

namespace Fijo.Code.SimpleOOPLang.Services {
	public class Container {
		public readonly IList<Expression> Expressions;

		public Container(IList<Expression> expressions) {
			Expressions = expressions;
		}
	}
}