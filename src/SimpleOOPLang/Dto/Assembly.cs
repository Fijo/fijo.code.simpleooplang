using System.Collections.Generic;

namespace Fijo.Code.SimpleOOPLang.Dto {
	public class Assembly
	{
		public readonly IList<Expression> Expressions;
		
		public Assembly(IList<Expression> expressions) {
			Expressions = expressions;
		}

		public Assembly(params Expression[] expressions) : this((IList<Expression>) expressions) {}
	}
}