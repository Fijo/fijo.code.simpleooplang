namespace Fijo.Code.SimpleOOPLang.Dto {
	public class NativeFunc : Expression {
		public readonly string Name;

		public NativeFunc(string name) {
			Name = name;
		}
	}
}