using System.Collections.Generic;

namespace Fijo.Code.SimpleOOPLang.Dto {
	public class Call : Expression {
		public readonly Expression Target;
		public readonly IList<Expression> Arguments;

		public Call(Expression target, IList<Expression> arguments) {
			Target = target;
			Arguments = arguments;
		}

		public Call(Expression target, params Expression[] arguments) : this(target, (IList<Expression>) arguments) {}
	}
}