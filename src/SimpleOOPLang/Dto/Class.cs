using System.Collections.Generic;

namespace Fijo.Code.SimpleOOPLang.Dto {
	public class Class : Expression {
		public IDictionary<string, Expression> Content;
		public IList<Class> InharanceFrom;
	}
}