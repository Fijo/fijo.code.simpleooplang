namespace Fijo.Code.SimpleOOPLang.Dto {
	public class Variable : Expression {
		public readonly string Name;
		public Instance Value;

		public Variable(string name) {
			Name = name;
		}

		internal Variable(string name, Instance value) : this(name) {
			Value = value;
		}
	}
}