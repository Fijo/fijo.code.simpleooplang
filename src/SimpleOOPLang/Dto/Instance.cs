using System.Collections.Generic;

namespace Fijo.Code.SimpleOOPLang.Dto {
	public class Instance : Expression {
		public readonly IDictionary<string, Expression> Content;

		public Instance(IDictionary<string, Expression> content) {
			Content = content;
		}
	}
}