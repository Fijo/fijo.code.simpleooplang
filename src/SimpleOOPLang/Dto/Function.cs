using System.Collections.Generic;

namespace Fijo.Code.SimpleOOPLang.Dto {
	public class Function : Expression {
		public readonly IList<string> Arguments;
		public readonly IList<Expression> Content;

		public Function(IList<string> arguments, IList<Expression> content) {
			Arguments = arguments;
			Content = content;
		}

		public Function(IList<string> arguments, params Expression[] content) : this(arguments, (IList<Expression>) content) {}
	}
}