using System;
using System.Collections.Generic;
using Fijo.Code.SimpleOOPLang.Dto;

namespace Fijo.Code.SimpleOOPLang.Services {
	public class Native {
		public virtual Expression Execute(string function, IList<Expression> arguments) {
			switch(function) {
				case "set":
					var variable = (Variable) arguments[0];
					variable.Value = (Instance) arguments[1];
					return variable;
				case "get":
					return ((Variable) arguments[0]).Value;
				case "if":
					var instance = InstanceToBool((Instance) arguments[0]);
					return arguments[instance ? 1 : 2];
				case "print":
					Console.WriteLine(InstanceToBool((Instance) arguments[0]));
					return null;
				case "void":
					return null;
			}
			throw new NotSupportedException(function);
		}

		protected bool InstanceToBool(Instance instance) {
			var content = instance.Content;
			if(content.ContainsKey("__native_bool_true")) return true;
			if(content.ContainsKey("__native_bool_false")) return false;
			throw new InvalidCastException();
		}
	}
}