﻿using Fijo.Code.SimpleOOPLang.Dto;

namespace Fijo.Code.SimpleOOPLang.Services
{
	public interface ICodeUtil
	{
		bool IsInstanceOrVariable(Expression arg);
	}
}