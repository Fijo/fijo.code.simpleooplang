﻿using Fijo.Code.SimpleOOPLang.Dto;

namespace Fijo.Code.SimpleOOPLang.Services
{
	public class CodeUtil : ICodeUtil
	{
		public bool IsInstanceOrVariable(Expression arg) {
			return arg is Instance || arg is Variable;
		}
	}
}