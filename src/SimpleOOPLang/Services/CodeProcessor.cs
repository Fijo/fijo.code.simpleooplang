using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Fijo.Code.SimpleOOPLang.Dto;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Delegates;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.StringReader;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using FijoCore.Infrastructure.LightContrib.Module.Stream.Obj.Reader;
using JetBrains.Annotations;

namespace Fijo.Code.SimpleOOPLang.Services {
	public class CodeProcessor {
		private readonly Native Native;
		private readonly ICodeUtil _codeUtil = new CodeUtil();

		public CodeProcessor(Native native) {
			Native = native;
		}

		public Expression PassObjects(Expression expression, Func<Expression, Expression> pass) {
			switch(expression.GetType().Name) {
				case "Variable":
					return pass(expression);
				case "Call":
					var call = (Call) expression;
					return new Call(PassObjects(call.Target, pass), call.Arguments.Select(x => PassObjects(x, pass)).ToArray());
			}
			return expression;
		}

		
		public void ExecuteAssembly(Assembly assembly) {
			HandleExpressions(assembly.Expressions);
		}

		public Expression HandleExpression(Expression expression) {
			switch(expression.GetType().Name) {
				case "Call":
					return HandleCall((Call) expression);
				case "Class":
					return HandleClass((Class) expression);
			}
			return expression;
		}

		public Expression HandleClass(Class cls) {
			return new Instance(cls.Content.ToDictionary(x => x.Key, x => x.Value));
		}

		public Expression HandleCall(Call call)
		{
			var evaluatedCall = GetEvaluatedCall(call);

			if(evaluatedCall.Target is NativeFunc) return HandleNative(evaluatedCall);
			return HandleDefaultCall(evaluatedCall);
		}

		private Call GetEvaluatedCall(Call call) {
			return new Call(call.Target, call.Arguments.Select(arg =>
			                                                   {
				                                                   while (!_codeUtil.IsInstanceOrVariable(arg)) arg = HandleExpression(arg);
				                                                   return arg;
			                                                   }).ToArray());
		}

		private Expression HandleNative(Call call) {
			var function = ((NativeFunc) call.Target).Name;
			return Native.Execute(function, call.Arguments);
		}

		private Expression HandleDefaultCall(Call call) {
			var function = GetFunction(call.Target);
			var argumentsToReplace = GetArgumentsToReplace(call.Arguments, function);
			var @return = GetAndAddReturn(argumentsToReplace);

			var execBody = function.Content.Select(x => PassObjects(x, expr => {
				                                                           var variable = expr as Variable;
				                                                           if (variable == null) return expr;

				                                                           Expression replacementVariable;
				                                                           if (!argumentsToReplace.TryGetValue(variable.Name, out replacementVariable)) return expr;
				                                                           return replacementVariable;
			                                                           }));

			HandleExpressions(execBody);

			return @return.Value;
		}

		private void HandleExpressions(IEnumerable<Expression> execBody) {
			foreach (var expression in execBody)
				HandleExpression(expression);
		}

		private Variable GetAndAddReturn(IDictionary<string, Expression> argumentsToReplace)
		{
			var @return = new Variable("__return");
			argumentsToReplace.Add("__return", @return);
			return @return;
		}

		private Function GetFunction(Expression target) {
			while (!(target is Function)) target = HandleExpression(target);
			return (Function) target;
		}

		private IDictionary<string, Expression> GetArgumentsToReplace(IList<Expression> arguments, Function function) {
			var index = 0;
			return function.Arguments.ToDictionary(arg => arg, arg => arguments[index++]);
		}
	}

	public class CodeSerializationContext
	{
		public StringBuilder StringBuilder { get; set; }
	}

	public class CodeDeserializationContext {
		public IStringReader StringReader { get; set; }
	}

	
	public interface ICodeSerializationService
	{
		void Serialize(CodeSerializationContext csc, object obj);
		object Deserialize(CodeDeserializationContext cdsc);
	}

	public interface ICodeSerialization<T>
	{
		void Serialize(CodeSerializationContext csc,  T obj);
		bool IsUsed(IEnumerable<char> chars);
		T Deserialize(CodeDeserializationContext cdsc);
	}

	public static class StartsWithExtention {
		public static bool StartsWith<T>([NotNull] this IEnumerable<T> me, [NotNull] ICollection<T> value) {
			return me.Take(value.Count - 1).SequenceEqual(value);
		}
	}

	public class ClassSerialization : ICodeSerialization<Class>
	{
		private readonly ICodeSerializationService _codeSerializationService;

		public ClassSerialization(ICodeSerializationService codeSerializationService) {
			_codeSerializationService = codeSerializationService;
		}

		public void Serialize(CodeSerializationContext csc, Class obj) {
			csc.StringBuilder.AppendFormat(@"{{ {0} }}", );
		}

		public bool IsUsed(IEnumerable<char> chars) {
			return chars.StartsWith("class".ToCharArray());
		}

		public Class Deserialize(CodeDeserializationContext cdsc) {
			_codeSerializationService.Deserialize(cdsc);
		}
	}

	
	public class ContainerSerialization : ICodeSerialization<Container>
	{
		private readonly ICodeSerializationService _codeSerializationService;

		public ContainerSerialization(ICodeSerializationService codeSerializationService) {
			_codeSerializationService = codeSerializationService;
		}

		#region Implementation of ICodeSerialization<Container>
		public void Serialize(CodeSerializationContext csc, Container obj) {
			var sb = csc.StringBuilder;
			sb.Append('{');
			obj.Expressions.ForEachAndBetween(x => _codeSerializationService.Serialize(csc, x),
			                                  () => sb.Append(','));
			sb.Append('}');
		}

		public bool IsUsed(IEnumerable<char> chars) {
			return chars.SkipWhileValues(' ', '\n', '\r', '\t').First() == '{';
		}

		public Container Deserialize(CodeDeserializationContext cdsc) {
			return new Container(GetContent(cdsc).Cast<Expression>().ToList());
		}

		private IEnumerable<object> GetContent(CodeDeserializationContext cdsc) {
			char next;
			do {
				yield return _codeSerializationService.Deserialize(cdsc);
				next = cdsc.StringReader.GetChars().SkipWhileValues(' ', '\n', '\r', '\t').First();
			} while (next == ';');
			if(next != '}') throw new UnexpectedIdenifierException();
		}
		#endregion
	}

	public class UnexpectedIdenifierException : Exception {}

	public class CodeSerialization
	{
		private IList<Type> _openTypes = new List<Type>();
		private IList<Expression> _expressions = new List<Expression>();
		private Expression CurrentExpression{get { return _expressions.Last(); }}

		public Expression Serialize()
		{
			
		}
	}
}