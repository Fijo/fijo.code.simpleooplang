﻿using System;
using System.Collections.Generic;
using Fijo.Code.SimpleOOPLang.Dto;
using Fijo.Code.SimpleOOPLang.Services;

namespace Fijo.Code.SimpleOOPLang {
	class Program {
		static void Main(string[] args) {
			var @do = new CodeProcessor(new Native());

			var trueClass = new Class();
			var falseClass = new Class();
			trueClass.Content = new Dictionary<string, Expression>
			{
				{"__native_bool_true", new NativeFunc("void")},
				{
					"__equals", new Function(new List<string> {"obj"}, 
						new Call(new NativeFunc("set"),
								new Variable("__return"),
								new Call(new NativeFunc("if"), new Variable("obj"),
										trueClass,
										falseClass)
							)
					)
				}
			};
			falseClass.Content = new Dictionary<string, Expression>
			{
				{"__native_bool_false", new NativeFunc("void")},
				{
					"__equals", new Function(new List<string> {"obj"},
							new Call(new NativeFunc("set"), 
									new Variable("__return"),
									new Call(new NativeFunc("if"), new Variable("obj"),
											falseClass,
											trueClass))
						)
				}
			};

			var assembly = new Assembly(new Call(new NativeFunc("print"), new Call(trueClass.Content["__equals"], falseClass)));

			@do.ExecuteAssembly(assembly);

			Console.ReadLine();
		}
	}
}
